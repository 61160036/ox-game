import java.util.InputMismatchException;
import java.util.Scanner;

public class ox_61160036 {
	static char[][] OXBoard = new char[3][3];
    static char turn = 'O';
    static int row = 0;
    static int col = 0;
    
    static void showWelcome() {
    	System.out.println("Welcome to OX game !!!");
    }
    
    static void showTurn() {
    	System.out.println("Turn "+turn);
    }
    static void showBoard(){
        System.out.println("______________________");
        for(int i = 0 ;i < 3;i ++){
            for(int j = 0 ;j < 3;j ++){
            	if(OXBoard[i][j]==0) {
            		System.out.print("-");
            	}else {
            		System.out.print(OXBoard[i][j]);
            	}
               System.out.print(" ");  
        }
            System.out.println("");
        }
    }
    static void checkInput(int row ,int col){
    	if(row < 3 && row > -1 && col < 3 && col > -1) {
            if(OXBoard[row][col]== 0) { 
             }else {
        	System.out.println("Sorry row, col is full "
        			+ "please input again:");
        	showBoard();
            input();
            }
            }else {
    			System.out.println("Sorry we don't have row, col in "
    					+ "this game please input again: ");
    			showBoard();
    			input();
    			}
    		}
    static void input(){
    	try {
    	System.out.print("Please Enter row, col: ");
    	Scanner k = new Scanner(System.in);
    	row = k.nextInt()-1;
        col = k.nextInt()-1;
        checkInput(row,col);
        OXBoard[row][col]=turn;
    }catch(InputMismatchException e) {System.out.println("Sorry it is "
    		+ "not a number please input again.");input();}}
    static void swiftTurn(){
    	if(turn=='O') {
    		turn ='X';
    	}else {
    		turn='O';
    	}
    }
    static void checkWinner(){
    	checkRow();
        checkCol();
        checkCross();
        checkDraw();
    }
    static void checkRow(){
    	for(int i = 0 ;i < 3 ;i ++) {
    		if(turn==OXBoard[i][0]) {
    			if(OXBoard[i][0]== OXBoard[i][1]&& OXBoard[i]
    					[0] == OXBoard[i][2]) {
    				showBoard();
    				System.out.println("______________________");
        			System.out.println("Player "+turn+" Win !!");
        		    System.exit(1);  
        		 	}
        		}
    		}
    	
    }
    static void checkCol(){
    	for(int i = 0 ;i < 3 ;i ++) {
    		if(turn==OXBoard[0][i]) {
    			if(OXBoard[0][i]== OXBoard[1][i]&& 
    					OXBoard[1][i] == OXBoard[2][i]) {
    				showBoard();
    				System.out.println("______________________");
        			System.out.println("Player "+turn+" Win !!");
        		    System.exit(1);  
        		 	}
        		}
    		}
    }
    static void checkCross(){
    	if(turn==OXBoard[1][1]) {
    		if(OXBoard[0][0]== OXBoard[1][1]&& 
    				OXBoard[1][1] == OXBoard[2][2]) {
    			showBoard();
    			System.out.println("______________________");
    			System.out.println("Player "+turn+" Win !!");
    		    System.exit(1);  
    		 	}else if(OXBoard[0][2]==OXBoard[1][1]&& OXBoard[1][1] == OXBoard[2][0]) {
    				showBoard();
    				System.out.println("______________________");
        			System.out.println("Player "+turn+" Win !!");
        		    System.exit(1);  
        		}
    	}
    }
    static void checkDraw(){
    	int draw = 0;
        for(int i = 0 ;i < 3;i ++){
            for(int j = 0 ;j < 3;j ++){
		if(OXBoard[i][j]==0) {              
		}else {
                   draw = draw+1; 
                }
            }
	}
        if(draw == 9){
        	showBoard();
        	System.out.println("======================");
            System.out.println("Draw!!");
            System.exit(1); 
        } 
    }
    public static void main(String[] oxtablegs) {
    	showWelcome();
        for(;;){
        showBoard();
        showTurn();
        input();
        checkWinner();
        swiftTurn();
        }
    }
    	
}
